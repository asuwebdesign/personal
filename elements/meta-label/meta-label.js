import React from "react"

const MetaLabel = props => {
  return (
    <div className="meta-label">{props.label}</div>
  )
}

export default MetaLabel
